Bibliothèque unicaen/authentification
=====================================

Ce module ajoute à une application la possibilité d'authentifier les utilisateurs.

Quatre modes d'authentification sont disponibles : 
- LDAP
- base de données 
- CAS
- shibboleth

Cette bibliothèque est le résultat de l'extraction des fonctionnalités d'authentification de l'ancienne bibliothèque
[unicaen/auth](https://git.unicaen.fr/lib/unicaen/auth).


## Documentation

- [Authentification](doc/authentification/auth.md)
