<?php

namespace UnicaenAuthentification;

use Laminas\EventManager\EventInterface;
use Laminas\ModuleManager\Feature\AutoloaderProviderInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\ModuleManager\Feature\ServiceProviderInterface;
use Laminas\Mvc\MvcEvent;
use UnicaenAuthentification\Event\Listener\LdapAuthenticationFailureLoggerListener;
use UnicaenAuthentification\Options\ModuleOptions;

/**
 * Point d'entrée du module d'authentification Unicaen.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ServiceProviderInterface, BootstrapListenerInterface
{
    /**
     * @var \UnicaenAuthentification\Options\ModuleOptions
     */
    private $moduleOptions;

    /**
     * @var \Laminas\EventManager\EventManagerInterface
     */
    private $eventManager;

    /**
     * @var \Laminas\ServiceManager\ServiceManager
     */
    private $serviceManager;

    /**
     * @return array
     * @see ConfigProviderInterface
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     * @see AutoloaderProviderInterface
     */
    public function getAutoloaderConfig()
    {
        return [
            'Laminas\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function onBootstrap(EventInterface $e)
    {
        if ($e instanceof MvcEvent) {
            /** @var \Laminas\Mvc\Application $application */
            $application = $e->getApplication();
            $this->serviceManager = $application->getServiceManager();
            $this->eventManager = $application->getEventManager();
            $this->moduleOptions = $this->serviceManager->get(ModuleOptions::class);

            $this->attachEventListeners();
        }
    }

    protected function attachEventListeners()
    {
        // log éventuel des erreurs d'authentification LDAP
        $logLdapAuthenticationFailure = $this->moduleOptions->getLdap()['log_failures'] ?? false;
        if ($logLdapAuthenticationFailure) {
            /** @var LdapAuthenticationFailureLoggerListener $listener */
            $listener = $this->serviceManager->get(LdapAuthenticationFailureLoggerListener::class);
            $listener->attach($this->eventManager);
        }
    }

    /**
     * @return array
     * @see ServiceProviderInterface
     */
    public function getServiceConfig()
    {
        return [
            //========== repris du module zf-commons/zfc-user-doctrine-orm abandonné =========
            'aliases' => array(
                'zfcuser_doctrine_em' => 'Doctrine\ORM\EntityManager',
            ),
            //===========================================


            'factories' => [
                //========== repris du module zf-commons/zfc-user-doctrine-orm abandonné =========
                'zfcuser_module_options' => function ($sm) {
                    $config = $sm->get('Configuration');
                    return new Options\ModuleOptions(isset($config['zfcuser']) ? $config['zfcuser'] : array());
                },
                //===========================================

            ],
        ];
    }
}