# CHANGELOG

6.4.5
-----

- Ajout d'une garde sur la fonction Provider/Identity/Chain::getAllIdentityRoles() pour éviter le cas où $role est null

6.4.4
-----

- Authentification shibboleth : serrages de vis dans les typages et les attributs obligatoires, toilettage, doc.
- Ajout de documentation

6.4.3 (29/01/2025)
------------------

- Dépendance ZfcUser explicite
- Remplacement de Laminas\Crypt\Password\Bcrypt par ZfcUser\Password\Bcrypt

6.4.2
-----

- [Fix] Aide de vue UserUsurpationHelper : manquait le use de classe AbstractUser.


6.4.1 (14/01/2025)
------------------

- [Fix] Désactivation de l'intranavigation sur le formulaire de connexion


6.4.0 (10/01/2025)
------------------

- Passage à phpCas 1.6
- [Fix] Correction de problème de version UnicaenApp


6.3.2 (09/01/2025)
------------------

- Compatibilité OK UnicaenUtilisateur 7.0

6.3.1
-----

- [Fix] Correction en cas de namesRole ne dépendant pas d'entité AbstractRole


6.3.0
-----

- Rattrapage des évolutions récentes de unicaen/auth : Le lien de déconnexion est désormais positionné dans le popover affichant les détails de l'utilisateur connecté.
- Rattrapage des évolutions récentes de unicaen/auth : Nouveau paramètre de config 'local.ldap.log_failures' permettant d'activer les logs d'erreur d'auth Ldap (LdapAuthenticationFailureLoggerListener).
- Rattrapage des évolutions récentes de unicaen/auth : Correction du .gitlab-ci.yml pour utiliser une image plus légère pour la mise à jour de Satis (packagist unicaen).
- Abandon officiel de la clé de config 'ldap_username' (en doublon depuis un moment avec la nouvelle clé local.ldap.username).
- L'instanciation manuelle de la classe ModuleOptions pour fusionner avec la config zfc-user était inutile : la factory fait la fusion !


6.0.4
-----

- [FIX] Utilisation à tort de l'option de config 'shib_user_id_extractor' sur des données d'usurpation/simulation.


## 6.0.3 (21/03/2023)

- [Fix] UserContext manipule des Laminas\Permissions\Acl\Role\RoleInterface, tous les rôles ne venant pas de la BDD


## 5.0.2

- Si CAS est le seul type d'authentification alors le lien de connexion envoie directement vers le CAS sans passer par la sélection du type


## 1.0.1 (07/02/2022)

- Ajout d'un paramètre "form_skip" qui permet de sauter la page d'authentification "/auth/connexion" si on utilise qu'une seule source d'authentification (pas utilisable pour une authentificatin locale).
- [Fix] La déconnexion du CAS n'était pas effective lors de la déconnexion de l'application

## 1.0.0 (02/02/2022)

- Scission du module UnicaenAuth en trois modules :
  - UnicaenAuthentification
  - UnicaenUtilisateur
  - UnicaenPrivilege

---

## UnicaenAuth (deprecated)

## 1.3.0 - 23/01/2019

- Authentification locale activable/désactivable dans la config.
    - Clé de config `unicaen-auth` > `local` > `enabled`.

- Ajout de la fonctionnalité "Mot de passe oublié" pour l'authentification locale. 
    - Principe: un lien contenant un token est envoyé par mail à l'utilisateur.
    - NB: Le username de l'utilisateur doit être une adresse électronique.
    - NB: nécessité de créer une nouvelle colonne dans la table des utilisateurs,
      cf. [répertoire data](./data).    

## 1.3.1 - 25/01/2019

- Fonctionnalité "Mot de passe oublié" :
    - Correction: l'utilisateur n'était pas recherché par son username!
    - Ajout d'un validateur sur le formulaire de saisie de l'adresse électronique.
    - Vérification que le compte utilisateur est bien local.

## 1.3.2 - 29/01/2019

- Authentification Shibboleth: possibilité de spécifier les attributs nécessaires au fonctionnement de l'appli 
  (clé de config `unicaen-auth` > `shibboleth` > `required_attributes`).
  
## 1.3.3 - 29/01/2019

- Correction du namespace de l'exception InvalidArgumentException lancée dans ShibService. 

## 1.3.4 - 30/01/2019

- Correction d'un nom erroné de variable passée à une vue : passwordReset.
- Correction config globale .dist: désactivation par défaut de l'auth locale pure ; commentaires plus précis.
- Adapter Db: suppression catch exception inexistante ; correction doc ; ajout initialisation de variable manquante.

## 1.3.5 - 04/02/2019

- Simplifications autour de la simulation d'une authentification shibboleth.
