<?php

namespace UnicaenAuthentification\View\Helper;

use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le bouton de connexion via Shibboleth,
 * si l'authentification Shibboleth est activée.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class CasConnectViewHelper extends AbstractConnectViewHelper
{
    const TYPE = 'cas';
    const TITLE = "Authentification centralisée";

    public function __construct()
    {
        $this->setType(self::TYPE);
    }
}