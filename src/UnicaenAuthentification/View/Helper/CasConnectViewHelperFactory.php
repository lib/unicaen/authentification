<?php

namespace UnicaenAuthentification\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;

class CasConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return CasConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getCas();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $title = $config['title'] ?? null;
        $description = $config['description'] ?? null;

        $helper = new CasConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setTitle($title ?? CasConnectViewHelper::TITLE);
        $helper->setDescription($description);

        return $helper;
    }
}