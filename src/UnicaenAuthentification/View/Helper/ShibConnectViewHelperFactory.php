<?php

namespace UnicaenAuthentification\View\Helper;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAuthentification\Options\ModuleOptions;

class ShibConnectViewHelperFactory
{
    public function __invoke(ContainerInterface $container): ShibConnectViewHelper
    {
        /** @var ModuleOptions $moduleOptions */
        try {
            $moduleOptions = $container->get('unicaen-auth_module_options');
        } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {

        }
        $config = $moduleOptions->getShib();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $title = $config['title'] ?? null;
        $description = $config['description'] ?? null;

        $helper = new ShibConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setTitle($title ?? ShibConnectViewHelper::TITLE);
        $helper->setDescription($description);

        return $helper;
    }
}