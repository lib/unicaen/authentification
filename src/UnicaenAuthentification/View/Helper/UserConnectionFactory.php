<?php

namespace UnicaenAuthentification\View\Helper;

use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Description of UserConnectionFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class UserConnectionFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $helperPluginManager)
    {
        return $this->__invoke($helperPluginManager, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config')['unicaen-auth'];
        $activeModes = [];
        foreach ($config['auth_types'] as $type) {
            if ($config[$type]['enabled']) $activeModes[] = $type;
        }
        $onlyCas = (count($activeModes) === 1 AND $activeModes[0] === 'cas');

        $authUserContext = $container->get('authUserContext');

        $uc = new UserConnection($authUserContext);
        $uc->onlyCas = $onlyCas;
        return $uc;
    }
}