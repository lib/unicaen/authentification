<?php
namespace UnicaenAuthentification\View\Helper;

/**
 * Aide de vue générant le lien et les infos concernant la connexion à l'application.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class AppConnection extends \UnicaenApp\View\Helper\AppConnection
{

    /**
     * Retourne le code HTML généré par cette aide de vue.
     *
     * @return string
     */
    public function __toString()
    {
        $connexion = [];

        if ($userCurrentHelper = $this->getView()->plugin('userCurrent')) {
            if ($html = (string) $userCurrentHelper) {
                $connexion[] = $html;
            }
        }
        if (($userConnectionHelper = $this->getView()->plugin('userConnection'))) {
            /** @var \UnicaenAuthentification\View\Helper\UserConnection $userConnectionHelper */
            // On ne dessine que le lien de Connexion.
            // Le lien de déconnexion est dessiné dans le popover dédié à l'utilisateur connecté.
            if ($html = $userConnectionHelper->addClass('btn btn-success')->renderConnection()) {
                $connexion[] = $html;
            }
        }

        return implode(' | ', $connexion);
    }
}