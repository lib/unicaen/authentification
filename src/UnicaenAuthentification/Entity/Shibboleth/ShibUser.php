<?php

namespace UnicaenAuthentification\Entity\Shibboleth;

use UnicaenAuthentification\Entity\PasswordConstant;
use Webmozart\Assert\Assert;
use ZfcUser\Entity\UserInterface;

class ShibUser implements UserInterface
{
    protected ?string $id = null;
    protected ?string $username = null;
    protected ?string $email = null;
    protected ?string $displayName = null;
    protected ?string $nom = null;
    protected ?string $prenom = null;
    protected ?string $civilite = null;
    protected int $state = 1;

    /**
     * Teste si une chaîne ressemble à un EPPN.
     */
    static public function isEppn(string $string): bool
    {
        if (($pos = strpos($string, '@')) === false) {
            return false;
        }

        $domain = substr($string, $pos + 1);
        if (filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) === false) {
            return false;
        }

        return true;
    }

    /**
     * Extrait le domaine de l'EPPN spécifié.
     */
    static public function extractDomainFromEppn(string $eppn): string
    {
        Assert::true(static::isEppn($eppn), "La chaîne suivante n'est pas un EPPN valide : " . $eppn);

        $parts = explode('@', $eppn);

        return $parts[1];
    }

    /**
     * Retourne la partie domaine DNS de l'EPPN.
     * Retourne par exemple "unicaen.fr" lorsque l'EPPN est "tartempion@unicaen.fr"
     */
    public function getEppnDomain(): string
    {
        return static::extractDomainFromEppn($this->getEppn());
    }

    /**
     * Retourne l'eduPersonPrincipalName (EPPN), ex: 'gauthierb@unicaen.fr'
     */
    public function getEppn(): string
    {
        return $this->getUsername();
    }

    /**
     * Spécifie l'eduPersonPrincipalName (EPPN).
     *
     * @param string $eppn eduPersonPrincipalName (EPPN), ex: 'gauthierb@unicaen.fr'
     */
    public function setEppn(string $eppn): void
    {
        Assert::true(static::isEppn($eppn), "La chaîne suivante n'est pas un EPPN valide : " . $eppn);

        $this->setUsername($eppn);
    }

    /**
     * Retourne l'identifiant unique de cet utilisateur.
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Spécifie l'identifiant unique de cet utilisateur.
     *
     * @param string $id
     */
    public function setId($id): void
    {
        $this->id = (string) $id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername($username): void
    {
        $this->username = $username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function setDisplayName($displayName): void
    {
        $this->displayName = $displayName;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(?string $civilite): void
    {
        $this->civilite = $civilite;
    }

    public function getPassword(): string
    {
        return PasswordConstant::PASSWORD_SHIB;
    }

    public function setPassword($password): void
    {
        // noop
    }

    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    public function __toString(): string
    {
        return $this->getDisplayName();
    }
}