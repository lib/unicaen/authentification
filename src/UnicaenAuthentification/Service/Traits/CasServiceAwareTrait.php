<?php

namespace UnicaenAuthentification\Service\Traits;

use UnicaenAuthentification\Service\CasService;

trait CasServiceAwareTrait
{
    /**
     * @var CasService
     */
    protected $casService;

    /**
     * @param CasService $casService
     */
    public function setCasService(CasService $casService)
    {
        $this->casService = $casService;
    }
}