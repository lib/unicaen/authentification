<?php

namespace UnicaenAuthentification\Service\Traits;

use UnicaenAuthentification\Service\UserContext;

/**
 * @author Unicaen
 */
trait UserContextServiceAwareTrait
{
    /**
     * @var UserContext
     */
    protected $serviceUserContext;

    /**
     * @param UserContext $serviceUserContext
     * @return self
     */
    public function setServiceUserContext(UserContext $serviceUserContext) : UserContext
    {
        $this->serviceUserContext = $serviceUserContext;
        return $this->serviceUserContext;
    }

    public function getServiceUserContext() : UserContext
    {
        return $this->serviceUserContext;
    }

}
