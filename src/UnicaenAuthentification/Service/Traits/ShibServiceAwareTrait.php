<?php

namespace UnicaenAuthentification\Service\Traits;

use UnicaenAuthentification\Service\ShibService;

trait ShibServiceAwareTrait
{
    protected ShibService $shibService;

    public function setShibService(ShibService $shibService): void
    {
        $this->shibService = $shibService;
    }
}