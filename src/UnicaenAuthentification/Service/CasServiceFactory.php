<?php

namespace UnicaenAuthentification\Service;

use Interop\Container\ContainerInterface;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenAuthentification\Options\ModuleOptions;

class CasServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $service = new CasService();

        /** @var User $userService */
        $userService = $container->get('unicaen-auth_user_service');
        $service->setUserService($userService);

        /** @var mixed $router */
        $router = $container->get('router');
        $service->setRouter($router);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $service->setOptions($moduleOptions);

        /** @var LdapPeopleMapper $ldapPeopleMapper */
        $ldapPeopleMapper = $container->get('ldap_people_mapper');
        $service->setLdapPeopleMapper($ldapPeopleMapper);

        return $service;
    }
}