<?php

namespace UnicaenAuthentification\Service;

use Assert\Assertion;
use Assert\AssertionFailedException;
use InvalidArgumentException;
use Laminas\Router\Http\TreeRouteStack;
use Laminas\Session\Container;
use UnicaenApp\Exception\LogicException;
use UnicaenApp\Exception\RuntimeException;
use UnicaenAuthentification\Entity\Shibboleth\ShibUser;
use UnicaenUtilisateur\Entity\Db\AbstractUser;

/**
 * Shibboleth service.
 *
 * @author Unicaen
 */
class ShibService
{
    const SHIB_USER_ID_EXTRACTOR = 'shib_user_id_extractor';

    const DOMAIN_DEFAULT = 'default';

    const KEY_fromShibUser = 'fromShibUser';
    const KEY_toShibUser = 'toShibUser';

    protected ?ShibUser $authenticatedUser = null;
    protected array $shibbolethConfig = [];
    protected array $usurpationAllowedUsernames = [];

    static public function apacheConfigSnippet(): string
    {
        return <<<EOS
<Location "/">
    AuthType Shibboleth
    ShibRequestSetting requireSession false
    Require shibboleth
</Location>
<Location "/auth/shibboleth">
        AuthType Shibboleth
        ShibRequestSetting requireSession true
        Require shibboleth
</Location>
EOS;
    }

    public function setShibbolethConfig(array $shibbolethConfig): void
    {
        $this->shibbolethConfig = $shibbolethConfig;
    }

    public function setUsurpationAllowedUsernames(array $usurpationAllowedUsernames): void
    {
        $this->usurpationAllowedUsernames = $usurpationAllowedUsernames;
    }

    public function getAuthenticatedUser(): ?ShibUser
    {
        if (! $this->isShibbolethEnabled()) {
            return null;
        }

        if ($this->authenticatedUser === null) {
            // D'ABORD activation éventuelle de la simulation
            $this->handleSimulation();
            // ENSUITE activation éventuelle de l'usurpation
            $this->handleUsurpation();

            if (! $this->isAuthenticated()) {
                return null;
            }

            $this->authenticatedUser = $this->createShibUserFromServerArrayData();
        }

        return $this->authenticatedUser;
    }

    private function isAuthenticated(): bool
    {
        return
            $this->getValueFromShibData('REMOTE_USER', $_SERVER) ||
            $this->getValueFromShibData('Shib-Session-ID', $_SERVER) ||
            $this->getValueFromShibData('HTTP_SHIB_SESSION_ID', $_SERVER);
    }

    public function isShibbolethEnabled(): bool
    {
        return
            array_key_exists('enabled', $this->shibbolethConfig) && (bool) $this->shibbolethConfig['enabled'] ||
            array_key_exists('enable', $this->shibbolethConfig) && (bool) $this->shibbolethConfig['enable'];
    }

    public function getShibbolethSimulate(): array
    {
        if (! array_key_exists('simulate', $this->shibbolethConfig) || ! is_array($this->shibbolethConfig['simulate'])) {
            return [];
        }

        return $this->shibbolethConfig['simulate'];
    }

    private function getShibbolethAliasFor(string $attributeName): ?string
    {
        if (! array_key_exists('aliases', $this->shibbolethConfig) ||
            ! is_array($this->shibbolethConfig['aliases']) ||
            ! isset($this->shibbolethConfig['aliases'][$attributeName])) {
            return null;
        }

        return $this->shibbolethConfig['aliases'][$attributeName];
    }

    /**
     * Retourne les alias des attributs spécifiés.
     * Si un attribut n'a pas d'alias, c'est l'attribut lui-même qui est retourné.
     */
    private function getAliasedShibbolethAttributes(array $attributeNames): array
    {
        $aliasedAttributes = [];
        foreach ($attributeNames as $attributeName) {
            $alias = $this->getShibbolethAliasFor($attributeName);
            $aliasedAttributes[$attributeName] = $alias ?: $attributeName;
        }

        return $aliasedAttributes;
    }

    /**
     * Retourne true si la simulation d'un utilisateur authentifié via Shibboleth est en cours.
     */
    public function isSimulationActive(): bool
    {
        if (array_key_exists('simulate', $this->shibbolethConfig) &&
            is_array($this->shibbolethConfig['simulate']) &&
            ! empty($this->shibbolethConfig['simulate'])) {
            return true;
        }

        return false;
    }

    /**
     * Retourne la liste des attributs requis.
     */
    private function getShibbolethRequiredAttributes(): array
    {
        if (! array_key_exists('required_attributes', $this->shibbolethConfig)) {
            return [];
        }

        return (array)$this->shibbolethConfig['required_attributes'];
    }

    private function getShibUserIdExtractorDefaultConfig(): array
    {
        return $this->getShibUserIdExtractorConfigForDomain(self::DOMAIN_DEFAULT);
    }

    /**
     * Retourne la config permettant d'extraire l'id à partir des attributs.
     */
    private function getShibUserIdExtractorConfigForDomain(string $domain): array
    {
        $key = self::SHIB_USER_ID_EXTRACTOR;
        if (! array_key_exists($key, $this->shibbolethConfig)) {
            throw new RuntimeException("Aucune config '$key' trouvée.");
        }

        $config = $this->shibbolethConfig[$key];

        if (! array_key_exists($domain, $config)) {
            return [];
        }

        return $config[$domain];
    }

    public function handleSimulation(): void
    {
        if (! $this->isSimulationActive()) {
            return;
        }

        try {
            $shibUser = $this->createShibUserFromSimulationData();
        } catch (AssertionFailedException $e) {
            throw new LogicException("Configuration erronée", null, $e);
        }

        $this->simulateAuthenticatedUser($shibUser);
    }

    /**
     * @throws AssertionFailedException
     */
    private function createShibUserFromSimulationData(): ShibUser
    {
        $data = $this->getShibbolethSimulate();

        $this->assertRequiredAttributesExistInData($data);

        $eppn = $this->getValueFromShibData('eppn', $data);
        $email = $this->getValueFromShibData('mail', $data);
        $displayName = $this->getValueFromShibData('displayName', $data);
        $givenName = $this->getValueFromShibData('givenName', $data);
        $surname = $this->getValueFromShibData('sn', $data);
        $civilite = $this->getValueFromShibData('supannCivilite', $data);

        Assertion::true(ShibUser::isEppn($eppn), "L'eppn '" . $eppn . "' n'est pas de la forme 'id@domaine' attendue (ex: 'tartempion@unicaen.fr')");

        $shibUser = new ShibUser();
        $shibUser->setEppn($eppn);
        // propriétés de UserInterface
        $shibUser->setUsername($eppn);
        $domain = $shibUser->getEppnDomain(); // possible uniquement après $shibUser->setEppn($eppn)
        $id = $this->extractShibUserIdValueForDomainFromShibData($domain, $data);
        $shibUser->setId($id);
        $shibUser->setDisplayName($displayName);
        $shibUser->setEmail($email);
        // autres propriétés
        $shibUser->setNom($surname);
        $shibUser->setPrenom($givenName);
        $shibUser->setCivilite($civilite);

        return $shibUser;
    }

    /**
     * Retourne true si les données stockées en session indiquent qu'une usurpation d'identité Shibboleth est en cours.
     */
    private function isUsurpationActive(): bool
    {
        return $this->getSessionContainer()->offsetExists(self::KEY_fromShibUser);
    }

    public function activateUsurpationOld(ShibUser $fromShibUser, ShibUser $toShibUser): self
    {
//        // le login doit faire partie des usurpateurs autorisés
//        if (! in_array($fromShibUser->getUsername(), $this->usurpationAllowedUsernames)) {
//            throw new RuntimeException("Usurpation non autorisée");
//        }

        $session = $this->getSessionContainer();
        $session->offsetSet(self::KEY_fromShibUser, $fromShibUser);
        $session->offsetSet(self::KEY_toShibUser, $toShibUser);

        return $this;
    }

    public function activateUsurpation(ShibUser $currentShibUser, AbstractUser $utilisateurUsurpe): self
    {
        if (! ShibUser::isEppn($utilisateurUsurpe->getUsername())) {
            // cas d'usurpation d'un compte local (db) depuis une authentification shib
            return $this;
        }

        $toShibUser = new ShibUser();
        $toShibUser->setEppn($utilisateurUsurpe->getUsername());
        $toShibUser->setId(uniqid()); // peut pas mieux faire pour l'instant
        $toShibUser->setDisplayName($utilisateurUsurpe->getDisplayName());
        $toShibUser->setEmail($utilisateurUsurpe->getEmail());
        $toShibUser->setNom('?');     // peut pas mieux faire pour l'instant
        $toShibUser->setPrenom('?');  // peut pas mieux faire pour l'instant

//        // le login doit faire partie des usurpateurs autorisés
//        if (! in_array($fromShibUser->getUsername(), $this->usurpationAllowedUsernames)) {
//            throw new RuntimeException("Usurpation non autorisée");
//        }

        $session = $this->getSessionContainer();
        $session->offsetSet(self::KEY_fromShibUser, $currentShibUser);
        $session->offsetSet(self::KEY_toShibUser, $toShibUser);

        return $this;
    }

    /**
     * Suppression des données stockées en session concernant l'usurpation d'identité Shibboleth.
     */
    public function deactivateUsurpation(): self
    {
        $session = $this->getSessionContainer();
        $session->offsetUnset(self::KEY_fromShibUser);
        $session->offsetUnset(self::KEY_toShibUser);

        return $this;
    }

    public function handleUsurpation(): self
    {
        if (! $this->isUsurpationActive()) {
            return $this;
        }

        $session = $this->getSessionContainer();

        /** @var ShibUser|null $toShibUser */
        $toShibUser = $session->offsetGet($key = self::KEY_toShibUser);
        if ($toShibUser === null) {
            throw new RuntimeException("Anomalie: clé '$key' introuvable");
        }

        $this->simulateAuthenticatedUser($toShibUser);

        return $this;
    }

    private function getSessionContainer(): Container
    {
        return new Container(ShibService::class);
    }

    private function getMissingRequiredAttributesFromData(array $data): array
    {
        $requiredAttributes = $this->getShibbolethRequiredAttributes();
        $missingAttributes = [];

        foreach ($requiredAttributes as $requiredAttribute) {
            // un pipe permet d'exprimer un OU logique, ex: 'sn|surname'
            $attributes = array_map('trim', explode('|', $requiredAttribute));
            // attributs aliasés
            $attributes = $this->getAliasedShibbolethAttributes($attributes);

            $found = false;
            foreach (array_map('trim', $attributes) as $attribute) {
                if (isset($data[$attribute])) {
                    $found = true;
                }
            }
            if (!$found) {
                // attributs aliasés, dont l'un au moins est manquant, mise sous forme 'a|b'
                $missingAttributes[] = implode('|', $attributes);
            }
        }

        return $missingAttributes;
    }

    /**
     * @throws InvalidArgumentException Des attributs sont manquants
     */
    private function assertRequiredAttributesExistInData(array $data): void
    {
        $missingAttributes = $this->getMissingRequiredAttributesFromData($data);

        if (!empty($missingAttributes)) {
            throw new InvalidArgumentException(
                "Les attributs suivants sont manquants : " . implode(', ', $missingAttributes));
        }
    }

    /**
     * Inscrit dans le tableau $_SERVER le nécessaire pour usurper l'identité d'un utilisateur
     * qui se serait authentifié via Shibboleth.
     *
     * @param ShibUser $shibUser Utilisateur dont on veut usurper l'identité.
     */
    public function simulateAuthenticatedUser(ShibUser $shibUser): void
    {
        // 'REMOTE_USER' (notamment) est utilisé pour savoir si un utilisateur est authentifié ou non
        $this->setServerArrayVariable('REMOTE_USER', $shibUser->getEppn());

        // pour certains attributs, on veut une valeur sensée!
        $this->setServerArrayVariable('eppn', $shibUser->getEppn());
        $this->setServerArrayVariable('supannEmpId', $shibUser->getId()); // ou bien 'supannEtuId', peu importe.
        $this->setServerArrayVariable('displayName', $shibUser->getDisplayName());
        $this->setServerArrayVariable('mail', $shibUser->getEppn());
        $this->setServerArrayVariable('sn', $shibUser->getNom());
        $this->setServerArrayVariable('givenName', $shibUser->getPrenom());
        $this->setServerArrayVariable('supannCivilite', $shibUser->getCivilite());
    }

    private function createShibUserFromServerArrayData(): ShibUser
    {
        try {
            $this->assertRequiredAttributesExistInData($_SERVER);
        } catch (InvalidArgumentException $e) {
            throw new RuntimeException('Des attributs Shibboleth obligatoires font défaut dans $_SERVER.', null, $e);
        }

        $eppn = $this->getValueFromShibData('eppn', $_SERVER);
        $mail = $this->getValueFromShibData('mail', $_SERVER);
        $displayName = $this->getValueFromShibData('displayName', $_SERVER);
        $surname = $this->getValueFromShibData('sn', $_SERVER) ?: $this->getValueFromShibData('surname', $_SERVER);
        $givenName = $this->getValueFromShibData('givenName', $_SERVER);
        $civilite = $this->getValueFromShibData('supannCivilite', $_SERVER);

        $shibUser = new ShibUser();
        $shibUser->setEppn($eppn);
        // propriétés de UserInterface
        $shibUser->setUsername($eppn);
        $domain = $shibUser->getEppnDomain(); // possible uniquement après $shibUser->setEppn($eppn)
        $id = $this->extractShibUserIdValueForDomainFromShibData($domain, $_SERVER);
        $shibUser->setId($id);
        $shibUser->setDisplayName($displayName);
        $shibUser->setEmail($mail);
        $shibUser->setPassword(null);
        // autres propriétés
        $shibUser->setNom($surname);
        $shibUser->setPrenom($givenName);
        $shibUser->setCivilite($civilite);

        return $shibUser;
    }

    private function extractShibUserIdValueForDomainFromShibData(string $domain, array $data): ?string
    {
        // En cas d'usurpation, le domaine du compte utilisateur usurpé (ex : 'unicaen.fr') peut différer de celui
        // du compte utilisateur usurpateur (ex : 'univ-lehavre.fr'). Or, la config Shibboleth du module exploitée
        // ci-dessous n'a de sens que sur des vraies données d'authentification (i.e celle du compte usurpateur),
        // donc pas la peine d'aller plus loin : on génère un id bidon.
        // En cas de simulation, on invente les données d'authentification donc c'est la même idée.
        if ($this->isUsurpationActive() || $this->isSimulationActive()) {
            return uniqid();
        }

        $config = $this->getShibUserIdExtractorConfigForDomain($domain);
        if (empty($config)) {
            $config = $this->getShibUserIdExtractorDefaultConfig();
            if (empty($config)) {
                throw new RuntimeException("Aucune config trouvée ni pour le domaine '$domain' ni par défaut.");
            }
        }

        foreach ($config as $array) {
            $name = $array['name'];
            $value = $this->getValueFromShibData($name, $data) ?: null;
            if ($value !== null) {
                $pregMatchPattern = $array['preg_match_pattern'] ?? null;
                if ($pregMatchPattern !== null) {
                    if (preg_match($pregMatchPattern, $value, $matches) === 0) {
                        throw new RuntimeException("Le pattern '$pregMatchPattern' n'a pas permis d'extraire une valeur de '$name'.");
                    }
                    $value = $matches[1];
                }

                return $value;
            }
        }

        return null;
    }

    /**
     * Retourne l'URL de déconnexion Shibboleth à laquelle est éventuellement concaténée l'URL *absolue*
     * de retour (après déconnexion) spécifiée.
     */
    public function getLogoutUrl(?string $returnAbsoluteUrl = null): string
    {
        if ($this->getShibbolethSimulate()) {
            return '/';
        }
        if ($this->getAuthenticatedUser() === null) {
            return '/';
        }

        $logoutUrl = $this->shibbolethConfig['logout_url'];

        if ($returnAbsoluteUrl) {
            $logoutUrl .= urlencode($returnAbsoluteUrl);
        }

        return $logoutUrl;
    }

    public function reconfigureRoutesForShibAuth(TreeRouteStack $router): void
    {
        if (! $this->isShibbolethEnabled()) {
            return;
        }

        $router->addRoutes([
            // remplace les routes existantes (cf. config du module)
            'zfcuser' => [
                'type'          => 'Literal',
                'priority'      => 1000,
                'options'       => [
                    'route'    => '/auth',
                    'defaults' => [
                        'controller' => 'zfcuser',
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'login' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/connexion[/:type]',
                            'defaults' => [
                                'controller' => 'zfcuser', // NB: lorsque l'auth Shibboleth est activée, la page propose
                                'action'     => 'login',   //     2 possibilités d'auth : LDAP et Shibboleth.
                            ],
                        ],
                    ],
                    'logout' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/:operation/shibboleth/',
                            'defaults' => [
                                'controller' => 'UnicaenAuthentification\Controller\Auth',
                                'action'     => 'shibboleth',
                                'operation'  => 'deconnexion'
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    private function getValueFromShibData(string $name, array $data): ?string
    {
        $key = $this->getShibbolethAliasFor($name) ?: $name;

        if (! array_key_exists($key, $data)) {
            return null;
        }

        return $data[$key];
    }

    private function setServerArrayVariable(string $name, ?string $value): void
    {
        $key = $this->getShibbolethAliasFor($name) ?: $name;

        $_SERVER[$key] = $value;
    }
}
