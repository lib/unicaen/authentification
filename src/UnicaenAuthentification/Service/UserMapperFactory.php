<?php

namespace UnicaenAuthentification\Service;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

class UserMapperFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManagerInterface $em */
        $em = $container->get('zfcuser_doctrine_em');

        /** @var ModuleOptions $options */
        $options = $container->get('zfcuser_module_options');

        return new UserMapper($em, $options);
    }
}
