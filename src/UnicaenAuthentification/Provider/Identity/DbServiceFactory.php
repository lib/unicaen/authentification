<?php

namespace UnicaenAuthentification\Provider\Identity;

use Laminas\Ldap\Exception\LdapException;
use Interop\Container\ContainerInterface;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Options\ModuleOptions;
use UnicaenUtilisateur\Service\Role\RoleService;
use Laminas\Ldap\Ldap;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Db identity provider factory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class DbServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $user             = $container->get('zfcuser_user_service');
        $identityProvider = new Db($user->getAuthService());
        $identityProvider->setHostLocalization($container->get('HostLocalization'));

        $unicaenAppOptions = $container->get('unicaen-app_module_options');
        /* @var $unicaenAppOptions ModuleOptions */

        $ldapOptions = $unicaenAppOptions->getLdap()['connection']['default']['params'] ?? [];
        if ($ldapOptions) {
            try {
                $ldap = new Ldap($ldapOptions);
                $ldap->connect();
            } catch (LdapException $e) {
                throw new RuntimeException(
                    "Impossible de se connecter à l'annuaire LDAP. Si aucune connexion LDAP n'est nécessaire, veuillez " .
                    "commenter les paramètres de config (connection > default > params).",
                    $e
                );
            }
            $identityProvider->setLdap($ldap);
        }

        $identityProvider->setRoleService($container->get(RoleService::class));

        $config            = $container->get('BjyAuthorize\Config');
        $identityProvider->setDefaultRole($config['default_role']);
        $identityProvider->setAuthenticatedRole($config['authenticated_role']);

        return $identityProvider;
    }
}