<?php

namespace UnicaenAuthentification\Provider\Identity;

use BjyAuthorize\Service\Authorize;
use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Event\EventManager;
use UnicaenAuthentification\Service\UserContext;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Intsancie une chaîne de fournisseurs d'identité.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ChainServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $chain = new Chain();

        /** @var UserContext $userContextService */
        $userContextService = $container->get('AuthUserContext');

        /* @var $authorizeService Authorize */
        $authorizeService = $container->get('BjyAuthorize\Service\Authorize');

        /** @var EventManager $eventManager */
        $eventManager = $container->get(EventManager::class);

        $chain->setUserContextService($userContextService);
        $chain->setAuthorizeService($authorizeService);
        $chain->setEventManager($eventManager);

        $providers = $this->computeProviders($container);

        foreach ($providers as $priority => $name) {
            $provider = $container->get($name);
            $chain->getEventManager()->attach('getIdentityRoles', [$provider, 'injectIdentityRoles'], $priority);
        }

        return $chain;
    }

    /**
     * @param ContainerInterface $serviceLocator
     * @return array
     */
    private function computeProviders(ContainerInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config'); //'unicaen-auth_module_options'
        if (! isset($config['unicaen-auth']['identity_providers']) || ! $config['unicaen-auth']['identity_providers']) {
            throw new \UnicaenApp\Exception\RuntimeException("Aucun fournisseur d'identité spécifié dans la config.");
        }

        $providers = (array) $config['unicaen-auth']['identity_providers'];

        // retrait du fournisseur 'Ldap' si l'auth Ldap est désactivée
        if (isset($config['unicaen-auth']['ldap']['enabled']) && ! $config['unicaen-auth']['ldap']['enabled']) {
            $key = array_search('UnicaenAuthentification\Provider\Identity\Ldap', $providers, true);
            if ($key !== false) {
                unset($providers[$key]);
            }
        }

        return $providers;
    }
}