<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use UnicaenAuthentification\Options\Traits\ModuleOptionsAwareTrait;
use Laminas\EventManager\EventInterface;
use Laminas\EventManager\EventManagerAwareTrait;
use Laminas\Stdlib\ResponseInterface as Response;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;

/**
 * Adapter d'authentification locale (ldap ou db).
 *
 * @author Unicaen
 */
class LocalAdapter extends AbstractAdapter
{
    use ModuleOptionsAwareTrait;
    use EventManagerAwareTrait;
    
    const TYPE = 'local';

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @inheritDoc
     */
//    public function authenticate(EventInterface $e): bool
    public function authenticate( $e): bool
    {
        // NB: Dans la version 3.0.0 de zf-commons/zfc-user, cette méthode prend un EventInterface.
        // Mais dans la branche 3.x, c'est un AdapterChainEvent !
        // Si un jour c'est un AdapterChainEvent qui est attendu, plus besoin de faire $e->getTarget().
        $event = $e->getTarget(); /* @var $event AdapterChainEvent */

        $type = $event->getRequest()->getPost()->get('type');
        if ($type !== $this->type) {
            return false;
        }

        $result = $this->getEventManager()->triggerUntil(function ($result) {
            return $result === true || $result instanceof Response;
        }, 'authenticate', $event);

        if ($result->stopped()) {
            $last = $result->last();
            if ($last === true || $last instanceof Response) {
                return $last;
            }
//            throw new Exception\AuthenticationEventException(
//                sprintf(
//                    'Auth event was stopped without a response. Got "%s" instead',
//                    is_object($result->last()) ? get_class($result->last()) : gettype($result->last())
//                )
//            );
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function logout(EventInterface $e)
    {
        $responseCollection = $this->getEventManager()->triggerUntil(function ($test) {
            return ($test instanceof Response);
        }, 'logout', $e);

        if ($responseCollection->stopped()) {
            if ($responseCollection->last() instanceof Response) {
                return $responseCollection->last();
            }
        }
    }
}