<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use Interop\Container\ContainerInterface;
use ZfcUser\Authentication\Adapter\Exception\OptionsNotFoundException;
use ZfcUser\Options\ModuleOptions;

class AdapterChainServiceFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): AdapterChain
    {
        $chain = new AdapterChain();

        $options = $this->getOptions($container);
        $enabledTypes = array_keys($options->getEnabledAuthTypes()); // types d'auth activés

        // on attache chaque adapter uniquement s'il est activé
        foreach ($options->getAuthAdapters() as $priority => $adapterName) {
            /** @var AbstractAdapter $adapter */
            $adapter = $container->get($adapterName);
            if (in_array($adapter->getType(), $enabledTypes)) {
                $adapter->attach($chain->getEventManager(), $priority);
            }
        }

        return $chain;
    }

    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * set options
     *
     * @param ModuleOptions $options
     * @return self
     */
    public function setOptions(ModuleOptions $options): self
    {
        $this->options = $options;
        return $this;
    }

    /**
     * get options
     *
     * @param ContainerInterface|null $container (optional) Service Locator
     * @return ModuleOptions $options
     */
    public function getOptions(ContainerInterface $container = null): ModuleOptions
    {
        if (!$this->options) {
            if (!$container) {
                throw new OptionsNotFoundException(
                    'Options were tried to retrieve but not set ' .
                    'and no service locator was provided'
                );
            }

            $this->setOptions($container->get('unicaen-auth_module_options'));
        }

        return $this->options;
    }
}
