<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use Interop\Container\ContainerInterface;
use Laminas\Authentication\Storage\Session;
use Laminas\View\Helper\ServerUrl;
use UnicaenApp\Mapper\Ldap\People as LdapPeopleMapper;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\User;

class CasAdapterFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Cas
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null)
    {
        $adapter = new Cas();
        $adapter->setStorage(new Session(Cas::class));

        $this->injectDependencies($adapter, $container);

//        /** @var EventManager $eventManager */
//        $eventManager = $container->get(EventManager::class);
//        $adapter->setEventManager($eventManager);
//        $userService = $container->get('unicaen-auth_user_service'); /* @var $userService \UnicaenAuthentification\Service\User */
//        $eventManager->attach('userAuthenticated', [$userService, 'userAuthenticated'], 100);
//        $eventManager->attach('clear', function() use ($adapter){
//            $adapter->getStorage()->clear();
//        });

        return $adapter;
    }

    /**
     * @param Cas $adapter
     * @param ContainerInterface $container
     */
    private function injectDependencies(Cas $adapter, ContainerInterface $container)
    {
        /** @var User $userService */
        $userService = $container->get('unicaen-auth_user_service');
        $adapter->setUserService($userService);

        /** @var mixed $router */
        $router = $container->get('router');
        $adapter->setRouter($router);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $adapter->setModuleOptions($moduleOptions);

        $substitut = $moduleOptions->getCas()['type'] ?? null;
        if ($substitut !== null) {
            $adapter->setType($substitut);
        }

        /** @var LdapPeopleMapper $ldapPeopleMapper */
        $ldapPeopleMapper = $container->get('ldap_people_mapper');
        $adapter->setLdapPeopleMapper($ldapPeopleMapper);

        /** @var ServerUrl $serverUrl */
        $serverUrl = $container->get('ViewHelperManager')->get('ServerUrl');

        $adapter->setServiceBaseUrl($serverUrl());
    }
}