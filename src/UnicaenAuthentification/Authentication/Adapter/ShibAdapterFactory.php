<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use Interop\Container\ContainerInterface;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Storage\Session;
use Laminas\Router\RouteInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\ShibService;
use UnicaenAuthentification\Service\User;

class ShibAdapterFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null): Shib
    {
        $adapter = new Shib();
        $adapter->setStorage(new Session(Shib::class));

        $this->injectDependencies($adapter, $container);

        return $adapter;
    }

    /**
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    private function injectDependencies(Shib $adapter, ContainerInterface $container): void
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);
        $adapter->setShibService($shibService);

        /** @var AuthenticationService $authenticationService */
        $authenticationService = $container->get('zfcuser_auth_service');
        $adapter->setAuthenticationService($authenticationService);

        /** @var RouteInterface $router */
        $router = $container->get('router');
        $adapter->setRouter($router);

        /** @var User $userService */
        $userService = $container->get('unicaen-auth_user_service');
        $adapter->setUserService($userService);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $adapter->setModuleOptions($moduleOptions);

        // type alias
        $substitut = $moduleOptions->getShib()['type'] ?? null;
        if ($substitut !== null) {
            $adapter->setType($substitut);
        }
    }
}