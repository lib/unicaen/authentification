<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use UnicaenAuthentification\Authentication\SessionIdentity;
use UnicaenAuthentification\Controller\AuthController;
use UnicaenAuthentification\Options\Traits\ModuleOptionsAwareTrait;
use UnicaenAuthentification\Service\Traits\ShibServiceAwareTrait;
use UnicaenAuthentification\Service\User as UserService;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Result as AuthenticationResult;
use Laminas\EventManager\EventInterface;
use Laminas\Http\Response;
use Laminas\Router\RouteInterface;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;

/**
 * CAS authentication adpater
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Shib extends AbstractAdapter
{
    use ModuleOptionsAwareTrait;
    use ShibServiceAwareTrait;

    const TYPE = 'shib';

    protected $type = self::TYPE;
    protected AuthenticationService $authenticationService;

    public function setAuthenticationService(AuthenticationService $authenticationService): self
    {
        $this->authenticationService = $authenticationService;
        return $this;
    }

    private RouteInterface $router;

    public function setRouter(RouteInterface $router): void
    {
        $this->router = $router;
    }

    private UserService $userService;

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * @param \Laminas\EventManager\EventInterface $e
     * @throws \Laminas\Authentication\Exception\ExceptionInterface
     */
    public function authenticate($e): Response|bool
    {
        // NB: Dans la version 3.0.0 de zf-commons/zfc-user, cette méthode prend un EventInterface.
        // Mais dans la branche 3.x, c'est un AdapterChainEvent !
        // Si un jour c'est un AdapterChainEvent qui est attendu, plus besoin de faire $e->getTarget().
        $event = $e->getTarget(); /** @var AdapterChainEvent $event */

        $type = $event->getRequest()->getPost()->get('type');
        if ($type !== $this->type) {
            return false;
        }

        /* DS : modification liée à une boucle infinie lors de l'authentification CAS */
        if ($this->isSatisfied()) {
            $storage = $this->getStorage()->read();
            $event
                ->setIdentity($storage['identity'])
                ->setCode(AuthenticationResult::SUCCESS)
                ->setMessages(['Authentication successful.']);
            return true;
        }

        $shibUser = $this->shibService->getAuthenticatedUser();

        if ($shibUser === null) {
            $redirectUrl = $this->router->assemble(['type' => 'shib'], [
                    'name' => 'zfcuser/authenticate',
                    'query' => ['redirect' => $event->getRequest()->getQuery()->get('redirect')]]
            );
            $shibbolethTriggerUrl = $this->router->assemble([], [
                    'name' => 'auth/shibboleth',
                    'query' => ['redirect' => $redirectUrl]]
            );
            $response = new Response();
            $response->getHeaders()->addHeaderLine('Location', $shibbolethTriggerUrl);
            $response->setStatusCode(302);

            return $response;
        }

        $identity = $this->createSessionIdentity($shibUser->getEppn());

        $event->setIdentity($identity);
        $this->setSatisfied(true);
        $storage = $this->getStorage()->read();
        $storage['identity'] = $event->getIdentity();
        $this->getStorage()->write($storage);
        $event
            ->setCode(AuthenticationResult::SUCCESS)
            ->setMessages(['Authentication successful.']);

        /* @var $userService UserService */
        $this->userService->userAuthenticated($shibUser);

        return true;
    }

    /**
     * @throws \Laminas\Authentication\Exception\ExceptionInterface
     */
    public function logout(EventInterface $e): ?Response
    {
        $storage = $this->getStorage()->read();

        if (is_array($storage)){
            if (! isset($storage['identity'])) {
                return null;
            }
        }else if (!$storage instanceof SessionIdentity) {
            return null;
        }

        parent::logout($e);

        // désactivation de l'usurpation d'identité éventuelle
        $this->shibService->deactivateUsurpation();

        /**
         * Si on souhaite que l'IDP Shibboleth gère la déconnexion
         * malgré qu'il puisse ne pas rediriger correctement (voir plus bas).
         */
        if ($this->moduleOptions->getShib()['idp_logout'] && $storage->getType() === 'shib') {
            // URL vers laquelle on redirige après déconnexion
            $returnUrl = $this->router->assemble([], [
                'name' => 'zfcuser/logout',
                'force_canonical' => true,
            ]);
            $shibbolethLogoutUrl = $this->shibService->getLogoutUrl($returnUrl);

            $response = new Response();
            $response->getHeaders()->addHeaderLine('Location', $shibbolethLogoutUrl);
            $response->setStatusCode(302);

            return $response;
        }

        /**
         * Problème : l'IDP Shibboleth ne redirige pas correctement vers l'URL demandée après déconnexion.
         * Solution : pour l'instant, on fait le nécessaire ici (qui devrait être fait normalement dans
         * {@see AuthController::logoutAction()} si l'IDP redonnait la main à l'appli.
         * todo: supprimer cette verrue lorsque l'IDP redirigera correctement.
         */
        $this->authenticationService->clearIdentity();

        return null;
    }
}