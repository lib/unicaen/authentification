<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use Laminas\Authentication\Storage\Session;
use Psr\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use ZfcUser\Mapper\UserInterface as UserMapperInterface;

class DbAdapterFactory
{
    /**
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container): Db
    {
        /** @var UserMapperInterface $userMapper */
        $userMapper = $container->get('zfcuser_user_mapper');

        $adapter = new Db();
        $adapter->setStorage(new Session(Db::class));
        $adapter->setMapper($userMapper);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $adapter->setModuleOptions($moduleOptions);

        $substitut = $moduleOptions->getDb()['type'] ?? null;
        if ($substitut !== null) {
            $adapter->setType($substitut);
        }

        return $adapter;
    }
}