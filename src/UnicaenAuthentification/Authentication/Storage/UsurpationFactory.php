<?php

namespace UnicaenAuthentification\Authentication\Storage;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use Laminas\Authentication\Storage\Session;
use Laminas\Session\SessionManager;
use ZfcUser\Mapper\UserInterface as UserMapper;

class UsurpationFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $moduleOptions
     * @return Usurpation
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $moduleOptions = null)
    {
        /** @var UserMapper $mapper */
        $mapper = $container->get('zfcuser_user_mapper');

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $storage = new Usurpation();
        $storage->setStorage(new Session(Usurpation::class, null, $sessionManager));
        $storage->setMapper($mapper);
        $storage->setModuleOptions($moduleOptions);

        return $storage;
    }
}