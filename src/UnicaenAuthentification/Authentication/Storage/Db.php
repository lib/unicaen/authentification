<?php

namespace UnicaenAuthentification\Authentication\Storage;

use UnicaenAuthentification\Authentication\SessionIdentity;
use ZfcUser\Entity\UserInterface;
use ZfcUser\Mapper\UserInterface as UserMapper;

/**
 * Db authentication storage.
 *
 * @author Unicaen
 */
class Db extends AbstractStorage
{
    const TYPE = \UnicaenAuthentification\Authentication\Adapter\Db::TYPE;

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @var UserMapper
     */
    protected $mapper;

    /**
     * @return bool
     */
    protected function isEnabled(): bool
    {
        $config = $this->moduleOptions->getDb();

        return isset($config['enabled']) && (bool) $config['enabled'];
    }
    
    /**
     * Recherche si les données mises en session à l'issue de l'authentification correspond à un utilisateur
     * dans la base de données.
     *
     * @return UserInterface|null
     */
    protected function findIdentity(): ?UserInterface
    {
        /** @var SessionIdentity $sessionIdentity */
        $sessionIdentity = $this->storage->read();
        $username = $sessionIdentity->getUsername();

        /**
         * Recherche de l'utilisateur authentifié dans la base de données.
         * Todo : quid si l'application n'utilise pas de table utilisateurs.
         */
        $identity = $this->getMapper()->findByUsername($username);
        if ($identity === null && is_numeric($username)) {
            // 2eme tentative des fois qu'il s'agisse d'un id
            $identity = $this->getMapper()->findById($username);
        }

        return $identity;
    }

    /**
     * @inheritDoc
     */
    protected function canHandleContents(SessionIdentity $sessionIdentity): bool
    {
        // Quel que soit le mode d'authentification (db, ldap, etc.), on recherchera systématiquement
        // l'éventuel utilisateur dans la base de données.
        return true;
    }

    /**
     * getMapper
     *
     * @return UserMapper
     */
    public function getMapper(): UserMapper
    {
        return $this->mapper;
    }

    /**
     * setMapper
     *
     * @param UserMapper|null $mapper
     * @return Db
     */
    public function setMapper(UserMapper $mapper = null): Db
    {
        $this->mapper = $mapper;
        return $this;
    }
}