<?php

namespace UnicaenAuthentification\Authentication\Storage;

use UnicaenAuthentification\Authentication\SessionIdentity;
use UnicaenAuthentification\Entity\Shibboleth\ShibUser;
use UnicaenAuthentification\Service\Traits\ShibServiceAwareTrait;

/**
 * Shibboleth authentication storage.
 *
 * @author Unicaen
 */
class Shib extends AbstractStorage
{
    use ShibServiceAwareTrait;

    const TYPE = \UnicaenAuthentification\Authentication\Adapter\Shib::TYPE;

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @var ShibUser
     */
    protected $resolvedIdentity;

    /**
     * @return bool
     */
    protected function isEnabled(): bool
    {
        $config = $this->moduleOptions->getShib();

        return isset($config['enabled']) && (bool) $config['enabled'];
    }

    /**
     * @return null|ShibUser
     */
    protected function findIdentity(): ?ShibUser
    {
        /** @var SessionIdentity $sessionIdentity */
        $sessionIdentity = $this->storage->read();
        $username = $sessionIdentity->getUsername();

        // L'identité en session doit ressembler à un EPPN.
        if (! ShibUser::isEppn($username)) {
            return null;
        }

        return $this->shibService->getAuthenticatedUser();
    }

    /**
     * @inheritDoc
     */
    public function clear(ChainEvent $e)
    {
        parent::clear($e);

        $this->shibService->deactivateUsurpation();
    }
}
