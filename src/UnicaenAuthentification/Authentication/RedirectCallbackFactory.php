<?php

namespace UnicaenAuthentification\Authentication;

use Interop\Container\ContainerInterface;
use Laminas\Mvc\Application;
use Laminas\Router\RouteInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use ZfcUser\Options\ModuleOptions;

class RedirectCallbackFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    { return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /* @var RouteInterface $router */
        $router = $container->get('Router');

        /* @var Application $application */
        $application = $container->get('Application');

        /* @var ModuleOptions $options */
        $options = $container->get('zfcuser_module_options');

        return new RedirectCallback($application, $router, $options);
    }
}
