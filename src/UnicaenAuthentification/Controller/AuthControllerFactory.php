<?php

namespace UnicaenAuthentification\Controller;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Form\LoginForm;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\ShibService;
use UnicaenAuthentification\Service\User as UserService;
use UnicaenAuthentification\Service\UserContext;
use ZfcUser\Controller\RedirectCallback;

class AuthControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return AuthController
     */
    public function __invoke(ContainerInterface $container): AuthController
    {
        /** @var ShibService $shibService */
        $shibService = $container->get(ShibService::class);

        /* @var $userService UserService */
        $userService = $container->get('unicaen-auth_user_service');

        /* @var $userContextService UserContext */
        $userContextService = $container->get(UserContext::class);

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /* @var RedirectCallback $redirectCallback */
        $redirectCallback = $container->get('zfcuser_redirect_callback');

        $controller = new AuthController();
        $controller->setShibService($shibService);
        $controller->setUserService($userService);
        $controller->setServiceUserContext($userContextService);
        $controller->setModuleOptions($moduleOptions);
        $controller->setRedirectCallback($redirectCallback);

        $authTypesConfig = $moduleOptions->getEnabledAuthTypes();
        foreach ($authTypesConfig as $type => $config) {
            /** @var LoginForm $loginForm */
            $loginForm = $container->get($config['form']);
            $controller->addLoginForm($loginForm);
        }

        return $controller;
    }
}