<?php

namespace UnicaenAuthentification\Options;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenApp\Exception\RuntimeException;

/**
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class ModuleOptionsFactory implements FactoryInterface
{
    protected string $class = ModuleOptions::class;

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $config       = $container->get('Configuration');
        $moduleConfig = $config['unicaen-auth'] ?? [];
        $moduleConfig = array_merge($config['zfcuser'], $moduleConfig);

        $this->validateConfig($moduleConfig);

        return new $this->class($moduleConfig);
    }

    protected function validateConfig(array $config): void
    {
        //
        // Config authentification shibboleth.
        //
        if (array_key_exists('shibboleth', $config)) {
            throw new RuntimeException(
                "Vous devez renommer la clé de configuration 'unicaen-auth.shibboleth' en 'unicaen-auth.shib'."
            );
        }
        if (array_key_exists('shib', $config)) {
            $shibConfig = $config['shib'];
            try {
                Assertion::keyExists($shibConfig, $k = 'logout_url');
            } catch (AssertionFailedException $e) {
                throw new RuntimeException(
                    "La clé de configuration obligatoire suivante n'a pas été trouvée : 'unicaen-auth.shib.$k'."
                );
            }
        }

        //
        // Config authentification locale (ldap et db).
        //
        if (! array_key_exists($k = 'local', $config)) {
            throw new RuntimeException(
                "La clé de configuration obligatoire suivante n'a pas été trouvée : 'unicaen-auth.$k'."
            );
        }

        //
        // Config authentification Db.
        //
        if (array_key_exists($k = 'db', $config)) {
            throw new RuntimeException(
                "La clé de config 'unicaen-auth.$k' et son contenu doivent être déplacés sous la nouvelle clé 'unicaen-auth.local'"
            );
        }

        //
        // Config authentification LDAP.
        //
        if (array_key_exists($k = 'ldap', $config)) {
            throw new RuntimeException(
                "La clé de config 'unicaen-auth.$k' et son contenu doivent être déplacés sous la nouvelle clé 'unicaen-auth.local'"
            );
        }

        //
        // Clé 'ldap_username' transformée.
        //
        if (array_key_exists($k = 'ldap_username', $config)) {
            throw new RuntimeException(
                "La clé de config 'unicaen-auth.$k' doit être supprimée et son contenu déplacé dans la nouvelle clé 'unicaen-auth.local.ldap.username'"
            );
        }
    }
}