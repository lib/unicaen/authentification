<?php

namespace UnicaenAuthentification\Options\Traits;

use UnicaenAuthentification\Options\ModuleOptions;

/**
 * @author Unicaen
 */
trait ModuleOptionsAwareTrait
{
    /**
     * @var ModuleOptions
     */
    protected $moduleOptions;

    /**
     * @param ModuleOptions $moduleOptions
     *
     * @return self
     */
    public function setModuleOptions(ModuleOptions $moduleOptions)
    {
        $this->moduleOptions = $moduleOptions;

        return $this;
    }

    /**
     * @return ModuleOptions
     */
    public function getModuleOptions()
    {
        return $this->moduleOptions;
    }
}